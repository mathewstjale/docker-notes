FROM    centos:7

# Update the system
RUN yum install -y sudo
# Update the system
RUN yum update -y

# Install Java JDK
#RUN yum install java-1.7.0-openjdk -y

# Install Java JDK
RUN yum install which -y


# Install Java JDK
RUN yum install rpm -y

# Install Java JRE
#RUN yum install java-1.7.0-openjdk-devel -y

# Install wget 
RUN yum install yum install wget -y

# Install wget 
RUN yum install yum install git -y

RUN mkdir -p /tmp/java
#CMD /tmp/java

#CMD wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" \
#"http://download.oracle.com/otn-pub/java/jdk/8u60-b27/jre-8u60-linux-x64.rpm" -P /tmp/java

#RUN yum localinstall /tmp/java/jre-8u60-linux-x64.rpm -y

## Java 8 jdk

#CMD wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u60-b27/jdk-8u60-linux-x64.rpm" -P /tmp/java

#RUN sudo yum localinstall /tmp/java/jdk-8u60-linux-x64.rpm -y


#PostgreSQL

RUN yum install -y https://download.postgresql.org/pub/repos/yum/10/redhat/rhel-7-x86_64/pgdg-centos10-10-1.noarch.rpm
RUN yum install postgresql10 -y
RUN yum install postgresql10-server -y

#MVN Install
COPY src/apache-maven-3.5.2-bin.tar.gz /usr/local
RUN gunzip /usr/local/apache-maven-3.5.2-bin.tar.gz
RUN tar -xvf /usr/local/apache-maven-3.5.2-bin.tar
RUN mv apache-maven-3.5.2 /usr/local/maven

## Jenkins

RUN wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat/jenkins.repo
RUN rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key

VOLUME ["/sys/fs/cgroup"]
VOLUME ["/data"]
#CMD export JAVA_HOME=/usr/bin/java
#CMD export PATH=$PATH:$JAVA_HOME
COPY docker-notes.md /tmp/
COPY src/jdk-8u152-linux-x64.tar.gz /tmp/java
COPY src/jenkins.war /tmp/java
COPY src/run_jenkins.sh /opt

RUN gunzip /tmp/java/jdk-8u152-linux-x64.tar.gz
RUN tar -xvf /tmp/java/jdk-8u152-linux-x64.tar
RUN mv /jdk1.8.0_152 /opt/java8 



RUN mv /tmp/java/jenkins.war /opt/
RUN chmod 755 /opt/run_jenkins.sh
#RUN nohup /opt/run_jenkins.sh > /dev/null 2>&1
#CMD ["/bin/bash","/opt/run_jenkins.sh > /dev/null 2>&1"]
CMD ["/bin/bash","/opt/run_jenkins.sh"]
#RUN cat /root/.jenkins/secrets/initialAdminPassword
#CMD ["/usr/sbin/init"]
#run_jenkins.sh

ENV JAVA_HOME="/opt/java8"
ENV M2_HOME="/usr/local/maven"
ENV PATH="$PATH:$JAVA_HOME/bin:$M2_HOME/bin"

#RUN nohup java -jar /opt/jenkins.war --httpPort=9080 > jenkins.log 2>&1

EXPOSE  9080

