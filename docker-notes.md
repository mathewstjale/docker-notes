# Docker Documentation as we go along

```shell
@author Makibane Tjale
        15 December 2017
```

## create a docker image file

```shell
touch Dockerfile
```

### Add in the flavour you want to run your container on by adding the FROM

```text
FROM    centos:7
```

### Add in what you want your container to have, you can add in shell commands, this commands will run when you build the docker image

```shell
# Enable Extra Packages for Enterprise Linux (EPEL) for CentOS
RUN     yum install -y epel-release
# Get jenkins repo file
COPY jenkins.repo /etc/yum.repos.d/

# Install rpm module
RUN yum install -y rpm

# Enable jenkins using key
RUN rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key

#Install jenkins
#RUN yum install jenkins -y

#Start Jenkins Dev Tool

EXPOSE  8080
CMD service jenkins start
```

### Build the image using the docker file created

```shell
docker build -t hubUserID/<imageName> . eg docker build -t makibanetjale/jenkins-test .
```

### once the image has been built, you can push it to hub.docker.com, note you must be logged in to docker before pushing

```shell
docker login --username=makibanetjale
docker push makibanetjale/<imageName>
```

### to pull an image from docker

```shell
docker pull makibanetjale/<imageName>
```

### run a created docker container

```shell
docker run -p 4000:80 -d makibanetjale/<containerName>
```