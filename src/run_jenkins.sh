#!/bin/sh

echo "Jenkins starting up"

nohup java -jar /opt/jenkins.war --httpPort=9080 > jenkins.log 2>&1
